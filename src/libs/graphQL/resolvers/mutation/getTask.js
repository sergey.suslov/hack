export default {
    patch: async (root, {input}, context) => {
        return await root[0].update(Object.assign({}, input));
    },
    delete: async (root, _, context) => {
        try {
            await root[0].destroy();
            return 'Successfully deleted!';
        } catch (e) {
            return 'Removal error!';
        }
    },
};

import controllers from '../../controllers/index';
import checkScopesAndResolve from '../../utils/checkScopesAndResolve';

const {getAllTasksByStudent, getAllTeamsByStudent} = controllers;

export default {
    tasks(args, _, context) {
        return checkScopesAndResolve(context, ['read:student', 'read:professor'], getAllTasksByStudent, args);
    },
    teams(args, _, context) {
        return checkScopesAndResolve(context, ['read:student', 'read:professor'], getAllTeamsByStudent, args);
    },
};

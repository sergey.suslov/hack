import controllers from '../../controllers/index';
import checkScopesAndResolve from '../../utils/checkScopesAndResolve';

const {getAllStudentsByTeam, getGroupByTeam} = controllers;

export default {
    students(args, _, context) {
        return checkScopesAndResolve(context, ['read:student', 'read:professor'], getAllStudentsByTeam, args);
    },
    group(args, _, context) {
        return checkScopesAndResolve(context, ['read:student', 'read:professor'], getGroupByTeam, args);
    },
};

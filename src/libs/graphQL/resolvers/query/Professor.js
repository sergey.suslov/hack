import checkScopesAndResolve from '../../utils/checkScopesAndResolve';
import controllers from '../../controllers';

const {getSubjectByProfessor} = controllers;

export default {
    subjects(args, _, context) {
        return checkScopesAndResolve(context, ['read:student', 'read:professor'], getSubjectByProfessor, args);
    },
};

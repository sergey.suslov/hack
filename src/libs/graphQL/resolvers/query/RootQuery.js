import controllers from '../../controllers/index';
import checkAuthAndResolve from '../../utils/checkAuthAndResolve';
import config from 'config';
import jwt from 'jsonwebtoken';
import db from '../../../sequelize';
import logger from '../../../../utils/logger';
import {AuthorizationError} from '../../errors';

const {getStudentByIdOrEmail} = controllers;

export default {
    student: (root, args, context) => {
        return checkAuthAndResolve(context, getStudentByIdOrEmail);
    },
    signInStudent: async (root, args, context) => {
        const {email, password} = args;
        let user;
        try {
            user = await db.Student.findOne({
                where: {
                    email,
                },
            });
        } catch (err) {
            logger.error(err);
            throw new AuthorizationError({
                message: `The user does not exist or the data is not correct.`,
            });
        }
        if (!user || !user.checkPassword(password)) {
            throw new AuthorizationError({
                message: `The user does not exist or the data is not correct.`,
            });
        }
        logger.info('User: ' + user.email + ' was founded!');
        if (user) {
            const payload = {
                user: {
                    id: user.id,
                    email: user.email,
                },
                scopes: [
                    'read:student',
                    'write:student',
                ],
            };

            return jwt.sign(
                payload,
                config.jwtSecret,
                {
                    expiresIn: 3600 * 24 * 7,
                },
            );
        } else {
            throw new AuthorizationError({
                message: `The user does not exist or the data is not correct.`,
            });
        }
    },
    signInProfessor: async (root, args, context) => {
        const {email, password} = args;
        let user;
        try {
            user = await db.Professor.findOne({
                where: {
                    email,
                },
            });
        } catch (err) {
            logger.error(err);
            throw new AuthorizationError({
                message: `The user does not exist or the data is not correct.`,
            });
        }
        if (!user || !user.checkPassword(password)) {
            throw new AuthorizationError({
                message: `The user does not exist or the data is not correct.`,
            });
        }
        logger.info('User: ' + user.email + ' was founded!');
        if (user) {
            const payload = {
                user: {
                    id: user.id,
                    email: user.email,
                },
                scopes: [
                    'read:student',
                    'write:student',
                ],
            };

            return jwt.sign(
                payload,
                config.jwtSecret,
                {
                    expiresIn: 3600 * 24 * 7,
                },
            );
        } else {
            throw new AuthorizationError({
                message: `The user does not exist or the data is not correct.`,
            });
        }
    },
};

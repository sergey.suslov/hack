import JSON from 'graphql-type-json';
import RootQuery from './query/RootQuery';
import Student from './query/Student';
import Task from './query/Task';
import Team from './query/Team';
import Group from './query/Group';
import Subject from './query/Subject';
import Professor from './query/Professor';
import RootMutation from './mutation/RootMutation';
import getProfessor from './mutation/getProfessor';
import getSubject from './mutation/getSubject';
import getGroup from './mutation/getGroup';
import getTeam from './mutation/getTeam';
import getStudent from './mutation/getStudent';
import getTask from './mutation/getTask';

module.exports = {
    JSON,
    getTask,
    getStudent,
    getTeam,
    getGroup,
    getSubject,
    getProfessor,
    RootMutation,
    RootQuery,
    Student,
    Task,
    Team,
    Group,
    Subject,
    Professor,
};

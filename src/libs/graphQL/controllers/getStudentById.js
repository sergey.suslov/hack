import db from '../../sequelize';

export default (root, args) => {
    return db.Student.findById(args.id);
};

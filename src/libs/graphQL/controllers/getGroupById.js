import db from '../../sequelize';

export default (root, args) => {
    return db.Group.findById(args.tag, {
        include: [db.Team, db.Subject],
    });
};

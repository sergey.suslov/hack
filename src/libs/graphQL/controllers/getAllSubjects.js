import db from '../../sequelize';

export default (root, args) => {
    return db.Subject.findAll({
        include: [db.Professor, db.Group],
    });
};

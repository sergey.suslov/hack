export default async function getSubjectByGroup(group) {
    return (await group.getSubjects())[0];
};

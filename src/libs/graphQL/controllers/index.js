import getAllGroups from './getAllGroups';
import getAllGroupsBySubject from './getAllGroupsBySubject';
import getGroupByTeam from './getGroupByTeam';
import getAllProfessors from './getAllProfessors';
import getAllStudents from './getAllStudents';
import getAllStudentsByTeam from './getAllStudentsByTeam';
import getAllSubjects from './getAllSubjects';
import getAllTasks from './getAllTasks';
import getAllTasksByStudent from './getAllTasksByStudent';
import getAllTeams from './getAllTeams';
import getAllTeamsByGroup from './getAllTeamsByGroup';
import getGroupById from './getGroupById';
import getProfessorById from './getProfessorById';
import getProfessorBySubject from './getProfessorBySubject';
import getStudentById from './getStudentById';
import getStudentByTask from './getStudentByTask';
import getSubjectByGroup from './getSubjectByGroup';
import getSubjectById from './getSubjectById';
import getSubjectByProfessor from './getSubjectByProfessor';
import getTaskById from './getTaskById';
import getTeamById from './getTeamById';
import getStudentByIdOrEmail from './getStudentByIdOrEmail';
import getAllTeamsByStudent from './getAllTeamsByStudent';
import getPreapostorByGroup from './getPreapostorByGroup';
import getProfessorByIdOrEmail from './getProfessorByIdOrEmail';

export default {
    getProfessorByIdOrEmail,
    getPreapostorByGroup,
    getAllTeamsByStudent,
    getStudentByIdOrEmail,
    getTeamById,
    getTaskById,
    getSubjectByProfessor,
    getSubjectById,
    getSubjectByGroup,
    getStudentByTask,
    getStudentById,
    getProfessorBySubject,
    getProfessorById,
    getGroupById,
    getAllTeamsByGroup,
    getAllTeams,
    getAllTasksByStudent,
    getAllTasks,
    getAllSubjects,
    getAllGroups,
    getAllGroupsBySubject,
    getGroupByTeam,
    getAllProfessors,
    getAllStudents,
    getAllStudentsByTeam,
};

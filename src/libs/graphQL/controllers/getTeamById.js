import db from '../../sequelize';

export default (root, args) => {
    return db.Team.findById(args.id, {
        include: [db.Student, db.Group],
    });
};

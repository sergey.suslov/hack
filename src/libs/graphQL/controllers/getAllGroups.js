import db from '../../sequelize';

export default (root, args) => {
    return db.Group.findAll({
        include: [db.Team],
    });
};

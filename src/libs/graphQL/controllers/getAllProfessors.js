import db from '../../sequelize';

export default (root, args) => {
    return db.Professor.findAll({
        include: [db.Subject],
    });
};

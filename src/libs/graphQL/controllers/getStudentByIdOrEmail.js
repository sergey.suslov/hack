import db from '../../sequelize';

export default function getSubjectByGroup(args) {
    return db.Student.findOne({
        where: args.user,
        include: [db.Task, db.Team],
    });
};

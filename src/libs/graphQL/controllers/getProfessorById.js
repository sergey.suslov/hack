import db from '../../sequelize';

export default (root, args) => {
    return db.Professor.findById(args.id, {
        include: [db.Subject],
    });
};

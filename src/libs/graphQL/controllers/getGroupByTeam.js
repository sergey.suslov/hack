export default async (team) => {
    return (await team.getGroups())[0];
};

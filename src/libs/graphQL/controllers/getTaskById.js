import db from '../../sequelize';

export default (root, args) => {
    return db.Task.findById(args.id);
};

export default async (professor) => await professor.getSubjects();

import directives from './directives';
import query from './query';
import Schema from './Schema';
import mutation from './mutation';

export default [
    directives,
    ...query,
    ...mutation,
    Schema,
];

export default `
  "Entry point for student"
  type RootQuery {
    "Search student by ID or email"
    student: Student
    signInStudent(email: String!, password: String!): String
    signInProfessor(email: String!, password: String!): String
  }
`;

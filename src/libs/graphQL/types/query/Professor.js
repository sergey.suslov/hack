const Professor = `
    type Professor {
        id: String!
        firstName: String
        lastName: String
        email: String
        about: String
        avatarURL: String
        subjects: [Subject]
    }
`;

export default Professor;

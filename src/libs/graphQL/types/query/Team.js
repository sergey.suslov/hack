const Team = `
    type Team {
        id: String!
        teamName: String
        students: [Student]
        group: Group
    }
`;

export default Team;

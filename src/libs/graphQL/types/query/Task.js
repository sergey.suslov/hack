const Task = `

    scalar JSON

    type Task {
        id: String!
        task: JSON
        status: Boolean
        mark: String
        student: Student
    }
`;

export default Task;

import Group from './Group';
import Praepostor from './Praepostor';
import Professor from './Professor';
import Student from './Student';
import RootQuery from './RootQuery';
import Subject from './Subject';
import Team from './Team';
import Task from './Task';

export default [
    Task,
    Team,
    Subject,
    RootQuery,
    Student,
    Professor,
    Praepostor,
    Group,
];

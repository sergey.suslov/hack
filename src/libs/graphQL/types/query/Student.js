const Student = `
    type Student {
        id: String!
        firstName: String
        lastName: String
        email: String!
        avatarURL: String
        tasks: [Task]
        teams: [Team]
    }
`;

export default Student;

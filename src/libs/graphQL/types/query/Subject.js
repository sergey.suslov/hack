const Subject =`
    type Subject {
        id: String!
        subjectName: String
        professor: Professor
        about: String
        filters: [String]
        groups: [Group]
    }
`;

export default Subject;

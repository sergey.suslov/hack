const getTask = `
    type getTask {
        delete: String
        patch(input: TaskInput): Task
    } 
`;

export default getTask;

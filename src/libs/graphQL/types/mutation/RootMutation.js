export default `
  "signUp"
  type RootMutation {
    "signUp for students"
    studentSignUp(input: StudentInput!): String
    "signUp for professors"
    professorSignUp(input: ProfessorInput!): String
    getProfessor: getProfessor
  }
`;

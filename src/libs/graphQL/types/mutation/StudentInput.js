export default `
  "Students signUp form"
  input StudentInput {
    email: String!
    firstName: String!
    lastName: String!
    password: String!
    avatarURL: String
  }
`;

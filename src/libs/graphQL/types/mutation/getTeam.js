const getTeam = `
    type getTeam {
        addStudentToTeam(input: TeamAddStudentInput!): Team
        getStudent(id: String!): getStudent
        delete: String
        patch(input: TeamInput): Team
    } 
`;

export default getTeam;

export default `
  "Students signUp form"
  input ProfessorInput {
    email: String
    firstName: String
    lastName: String
    password: String
    avatarURL: String
    about: String
  }
`;

export default `
  input SubjectInput {
    subjectName: String!
    about: String!
    filters: [String]
  }
`;

import config from 'config';
import jwt from 'jsonwebtoken';
import {AuthorizationError} from './../errors';

const checkScopesAndResolve = (
    context,
    expectedScopes,
    controller,
    ...params
) => {
    const token = context.headers.authorization;
    if (!token) {
        throw new AuthorizationError({
            message: `You must supply a JWT for authorization!`,
        });
    }
    const decoded = jwt.verify(
        // token.replace('Bearer ', ''),
        token,
        config.jwtSecret,
    );
    const scopes = decoded.scopes;
    if (!scopes) {
        throw new AuthorizationError({message: 'No scopes supplied!'});
    }
    if (scopes && expectedScopes.some((scope) => scopes.indexOf(scope) !== -1)) {
        return controller.apply(this, params);
    } else {
        throw new AuthorizationError({
            message: `You are not authorized. Expected scopes: ${expectedScopes.join(', ')}`,
        });
    }
};

export default checkScopesAndResolve;

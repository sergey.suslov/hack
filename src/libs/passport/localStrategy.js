import passport from 'koa-passport';
import LocalStrategy from 'passport-local';
import {Professor, Student} from '../sequelize';
import logger from '../../utils/logger';

passport.use('student', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
        session: false,
    },
    async function(req, email, password, done) {
        let user;
        try {
            user = await Student.findOne({where: {
                    email: email,
                }});
        } catch (err) {
            logger.error(err);
            return done(err);
        }
        if (!user || !user.checkPassword(password)) {
            logger.error('User does not exist, or invalid password');
            return done(null, false);
        }
        logger.info('User: ' + user.email +' was founded!');
        done(null, user);
    },
));

passport.use('professor', new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true,
        session: false,
    },
    async function(req, email, password, done) {
        let user;
        try {
            user = await Professor.findOne({where: {
                    email: email,
                }});
        } catch (err) {
            logger.error(err);
            return done(err);
        }
        if (!user || !user.checkPassword(password)) {
            logger.error('User does not exist, or invalid password');
            return done(null, false);
        }
        logger.info('User: ' + user.email +' was founded!');
        done(null, user);
    },
));

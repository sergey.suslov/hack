require('babel-core/register');
require('babel-polyfill');
import config from 'config';
import Koa from 'koa';
import path from 'path';
import fs from 'fs';
import log from './utils/logger';


const app = new Koa();
app.keys = [config.secret];

// middlewares
const middlewares = fs.readdirSync(path.join(__dirname, 'handlers')).sort();

middlewares.forEach(function(middleware) {
    app.use(require('./handlers/' + middleware));
});

// routes
app.use(require('./routes').routes());
app.use(require('./routes').allowedMethods());

app.listen(config.port);
log.warn(`Server run on port ${config.port}`);

import {Professor} from '../../libs/sequelize';

exports.get = async function(ctx) {
    let professor;
    try {
        professor = await Professor.findOne({
            where: {
                email: ctx.params.email,
            },
            attributes: [
                'id',
                'firstName',
                'lastName',
                'email',
                'group',
                'avatarURL',
            ],
        });
    } catch (err) {
        ctx.body = 'not found';
    }
    ctx.body = professor;
};

import {Student} from '../../libs/sequelize';

exports.get = async function(ctx) {
    let students;
    try {
       students = await Student.findAll({
           attributes: [
               'id',
               'firstName',
               'lastName',
               'email',
               'avatarURL',
           ],
       });
    } catch (err) {
        ctx.body = 'not found';
    }
    ctx.body = students;
};

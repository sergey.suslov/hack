import {Professor} from '../../libs/sequelize';

exports.get = async function(ctx) {
    let professors;
    try {
        professors = await Professor.findAll({
            attributes: [
                'id',
                'firstName',
                'lastName',
                'about',
                'email',
                'avatarURL',
            ],
        });
    } catch (err) {
        ctx.body = 'not found';
    }
    ctx.body = professors;
};

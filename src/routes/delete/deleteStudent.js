import {Student} from '../../libs/sequelize';

exports.delete = async function(ctx) {
    let student;
    try {
        student = await Student.findOne({
            where: {
                id: ctx.params.id,
            },
        });
    } catch (err) {
        ctx.body = 'not found';
        return;
    }
    try {
        await student.destroy();
        ctx.status = 200;
    } catch (e) {
        ctx.status = 404;
    }
};

import passport from 'koa-passport';
import jwt from 'jsonwebtoken';
import config from 'config';

exports.post = async function(ctx, next) {
    await passport.authenticate('student')(ctx, next);
    if (ctx.state.user) {
        const payload = {
            user: {
                id: ctx.state.user.id,
                email: ctx.state.user.email,
            },
            scopes: [
                'read:student',
                'write:student',
            ],
        };

        const token = jwt.sign(
            payload,
            config.jwtSecret,
            {
                expiresIn: 3600 * 24 * 7,
            },
            );
        ctx.body = {
            token: token,
        };
    } else {
        ctx.status = 401;
        ctx.body = {error: 'Invalid credentials'};
    }
};

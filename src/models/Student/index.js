import config from 'config';
import crypto from 'crypto';

module.exports = (sequelize, DataTypes) => {
    const Student = sequelize.define('student', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        firstName: {
            type: DataTypes.STRING,
            required: true,
            validate: {
                isAlpha: true,
                len: [2, 30],
            },
        },
        lastName: {
            type: DataTypes.STRING,
            required: true,
            validate: {
                isAlpha: true,
                len: [2, 30],
            },
        },
        email: {
            type: DataTypes.STRING,
            unique: true,
            required: true,
            validate: {
                isEmail: true,
            },
        },
        passwordHash: {
            type: DataTypes.STRING,
            required: true,
            set(password) {
                if (password !== undefined) {
                    if (password.length < 4) {
                        throw new Error('The password must contain more than 4 characters');
                    }
                }
                this._plainPassword = password;
                const salt = crypto.randomBytes(config.crypto.hash.length).toString('base64');

                if (password) {
                    this.setDataValue('salt', salt);
                    const hashPassword = crypto.pbkdf2Sync(
                        password,
                        salt,
                        12000,
                        config.crypto.hash.length,
                        'sha256'
                    ).toString('base64');
                    this.setDataValue('passwordHash', hashPassword);
                } else {
                    this.setDataValue('passwordHash', undefined);
                    this.setDataValue('salt', undefined);
                }
            },
            get() {
                return this._plainPassword;
            },
        },
        salt: {
            type: DataTypes.STRING,
            required: true,
        },
        avatarURL: {
            type: DataTypes.STRING,
            validate: {
                isUrl: true,
            },
        },
    }, {
        indexes: [
            {
                fields: ['email', 'id'],
            },
        ],
    });

    Student.associate = (db) => {
        Student.hasMany(db.Task, {
            foreignKey: 'studentId',
            constraints: false,
        });
        Student.hasMany(db.Message, {
            foreignKey: 'studentId',
            constraints: false,
            allowNull: false,
        });
        Student.hasOne(db.Group, {
           foreignKey: 'praepostor',
        });
        Student.belongsToMany(db.Team, {
            through: 'teamMember',
            constraints: false,
        });
    };

    Student.prototype.checkPassword = function(password) {
        const passwordHash = this.getDataValue('passwordHash');
        const salt = this.getDataValue('salt');
        if (!password) return false;
        if (!passwordHash) return false;
        return crypto.pbkdf2Sync(
            password,
            salt,
            12000,
            config.crypto.hash.length,
            'sha256'
        ).toString('base64') === passwordHash;
    };
    return Student;
};

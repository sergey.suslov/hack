module.exports = (sequelize, DataTypes) => {
    const Team = sequelize.define('team', {
        id: {
            type: DataTypes.UUID,
            defaultValue: DataTypes.UUIDV4,
            primaryKey: true,
        },
        teamName: {
            type: DataTypes.STRING,
            required: true,
        },
    }, {
        indexes: [
            {
                fields: ['id'],
            },
        ],
        timestamps: false,
    });
    Team.associate = (db) => {
        Team.belongsToMany(db.Student, {
            through: 'teamMember',
            constraints: false,
        });
        Team.belongsToMany(db.Group, {
            through: 'groupMember',
            constraints: false,
        });
    };
    return Team;
};

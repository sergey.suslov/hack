/* eslint-disable */
const gulp = require("gulp");
const babel = require("gulp-babel");
const nodemon = require('gulp-nodemon');
const childProcess = require('child_process');
const path = require('path');
const clean = require('gulp-clean');
// var concat = require("gulp-concat");
// var uglify = require("gulp-uglifyjs");

//to enable Uglify mod add:
// .pipe(uglify())
// .pipe(concat("server.js"))


const jest = require('gulp-jest').default;

gulp.task('buildTest', function () {
    return gulp.src('__tests__').pipe(jest({
        "preprocessorIgnorePatterns": [
            "dist/", "node_modules/"
        ],
        "automock": false
    }));
});

gulp.task('clean', function () {
    return gulp.src('dist', {read: false})
        .pipe(clean());
});

gulp.task('forceBuild', ['clean'], function () {
    return gulp.src("src/**/*.js")
        .pipe(babel())
        .pipe(gulp.dest("dist"));
});

gulp.task('build', function () {
    return gulp.src("src/**/*.js")
        .pipe(babel())
        .pipe(gulp.dest("dist"));
});

gulp.task('server', function (done) {

    nodemon({
        script: './dist/server.js', // <-- your application
        watch: ['./dist/**/*'],
        stdout: false // <-- important, otherwise output won't get piped properly
    })
        .on('readable', function () {
            // Pass output through bunyan formatter
            const bunyan = childProcess.fork(
                path.join('.', 'node_modules', 'bunyan', 'bin', 'bunyan'),
                ['--output', 'simple', '--color'], // <-- any of the CLI options that you prefer
                {silent: true}
            );

            bunyan.stdout.pipe(process.stdout);
            bunyan.stderr.pipe(process.stderr);
            this.stdout.pipe(bunyan.stdin);
            this.stderr.pipe(bunyan.stdin);
        });

    done();
});

gulp.task("start", function () {
    gulp.watch('src/**/*.js', ['build']);
});